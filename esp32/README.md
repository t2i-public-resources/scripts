# T2I esp-idf install script

Este script realiza clona o repositório do [esp-idf](https://github.com/espressif/esp-idf) e aplica os patches
para habilitar o protocolo [snmp](http://www.net-snmp.org).

Atualmente o script realiza o clone do repositório com a release 4.1. Para alterar a versão de download basta modificar a variável **VERSION** do script.

