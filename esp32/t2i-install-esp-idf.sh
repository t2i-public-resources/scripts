# T2I esp-idf install script

# Copyright (C) 2020 T2I Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# author: Calebe94
# email: calebe94@disroot.org

#!/bin/bash

#------------------#
# Global variables #
#------------------#
GIT_COMMAND="git clone --recursive -b"
ESP_IDF_URL="https://github.com/espressif/esp-idf.git"
VERSION=release/v4.1

#---------------#
# esp-idf patch #
#---------------#
ESP_IDF_PATCH_URL="https://gitlab.com/t2i-public-resources/patches/snippets/1982318/raw" # URL to download the t2i-esp-idf patch file.
ESP_IDF_PATCH_TMP=/tmp/t2i_esp-idf_snmp_enable.patch # Path to store the t2i-esp-idf patch file temporarily.
ESP_IDF_PATCH_PATH=$IDF_PATH # Path where the patch will be applyied.

#------------#
# lwip Patch #
#------------#
#LWIP_PATCH_URL="https://gitlab.com/t2i-public-resources/patches/snippets/1975951/raw" # URL to download the t2i-esp-idf patch file.
#LWIP_PATCH_TMP=/tmp/t2i-lwip.patch # Path to store the t2i-esp-idf patch file temporarily.
#LWIP_PATCH_PATH=$IDF_PATH/components/lwip/lwip # Path where the patch will be applyied.


# Verify if the environment variable IDF_PATH exists.
# If the variable exists, esp-idf was installed at least once in the computer. So we should verify if the folder IDF_PATH exists.
# If the variable does not exists it is because:
# 1. IDF_PATH was no setted.
# 2. esp-idf is not installed.

if [ -d $IDF_PATH ]; then
  echo "$IDF_PATH exists."
  echo "Moving to $IDF_PATH.old..."
  ([ -d $IDF_PATH.old ] && rm -fr $IDF_PATH.old )
  mv $IDF_PATH $IDF_PATH.old
else
  echo "esp-idf is not installed."
  ([ ! -d $HOME/esp ] && echo "$HOME/esp/ folder does not exists. Creating $HOME/esp/..." && mkdir $HOME/esp/)
  echo "Resume..."
fi

echo "Cloning espressif/esp-idf repository ..."

cd $HOME/esp/ && $GIT_COMMAND $VERSION $ESP_IDF_URL

echo "Downloading T2I esp-idf patch ..."

curl -L $ESP_IDF_PATCH_URL > $ESP_IDF_PATCH_TMP

echo "Applying T2I esp-idf patch ..."

cd $ESP_IDF_PATCH_PATH && git apply $ESP_IDF_PATCH_TMP && cd - &> /dev/null

echo "OK."

#echo "Downloading T2I lwip patch ..."

#curl -L $LWIP_PATCH_URL > $LWIP_PATCH_TMP

#echo "Applying T2I lwip patch ..."

#cd $LWIP_PATCH_PATH && git apply $LWIP_PATCH_TMP  && cd - &> /dev/null

#echo "OK."

echo "Done!"
